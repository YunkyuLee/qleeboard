const express = require('express')
const router = express.Router()
const models = require('../models')
const Op = models.Sequelize.Op

// 알람 카운트
router.get('/', async (req, res, next) => {
  try {
    const user = req.user
    const userSrl = user._srl
    const alarm = await models.Alarm.findAndCountAll({
      where: {
        user_srl: userSrl,
        comments_count: { [Op.gt]: models.sequelize.col('comments_check_count') }
      },
      include: [
        { model: models.Post,
          where: {
            id: models.sequelize.col('post_id')
          },
          required: true
        }
      ]
    })
    res.json({
      result: alarm,
      message: 'success'
    })
  } catch (e) {
    res.status(403).json({
      message: 'error',
      error: e.message
    })
  }
})

// 알람 확인
router.get('/check', async (req, res, next) => {
  try {
    const user = req.user
    const userSrl = user._srl
    const postId = req.query.postId
    const alarm = await models.Alarm.update(
      {
        comments_check_count: models.sequelize.col('comments_count')
      },
      {
        where: {
          user_srl: userSrl,
          post_id: postId
        }
      })
    res.json({
      result: alarm,
      message: 'success'
    })
  } catch (e) {
    res.json({
      message: 'error',
      error: e.message
    })
  }
})

// 알람 보내기
router.post('/:post_id', async (req, res, next) => {
  try {
    res.json({
      message: 'Thank you'
    })
    const params = req.params
    const postId = params.post_id
    const user = req.user
    const userSrl = user._srl

    const postAuthor = await models.Post.findOne({
      attributes: ['user_srl'],
      where: {
        id: postId
      }
    })
    const commentsIds = await models.PostComment.findAll({
      attributes: ['user_srl'],
      where: {
        post_id: postId
      }
    })
    let idArr = [...new Set([...commentsIds.map(v => v.getDataValue('user_srl')), postAuthor.getDataValue('user_srl')])]

    idArr.forEach(async id => {
      const result = await models.Alarm.findOrCreate({
        where: { user_srl: id, post_id: postId },
        defaults: {
          user_srl: id,
          post_id: postId,
          comments_count: commentsIds.length
        }
      })
      const resultId = result[0].getDataValue('id')
      if (id !== userSrl) {
        await models.Alarm.update(
          {
            comments_count: commentsIds.length
          },
          {
            where: { id: resultId }
          })
      } else {
        await models.Alarm.update(
          {
            comments_count: commentsIds.length,
            comments_check_count: commentsIds.length
          },
          {
            where: { id: resultId }
          })
      }

    })
  } catch (e) {
    console.log(e)
  }
})

module.exports = router
