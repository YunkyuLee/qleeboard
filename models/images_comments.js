module.exports = function (sequelize, DataTypes) {
  const commentsImage = sequelize.define('CommentImage',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      comment_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      img_url: {
        type: DataTypes.STRING(250),
        allowNull: false
      }
    },
    {
      // don't use camelcase for automatically added attributes but underscore style
      // so updatedAt will be updated_at
      underscored: true,
      // disable the modification of tablenames; By default, sequelize will automatically
      // transform all passed model names (first parameter of define) into plural.
      // if you don't want that, set the following
      freezeTableName: true,
      paranoid: true,
      // define the table's name
      tableName: 'images_comments',
      timestamps: true
    })

  commentsImage.associate = (models) => {
    models.CommentImage.belongsTo(models.PostComment, {
      foreignKey: 'comment_id',
      targetKey: 'id'
    })
  }

  return commentsImage
}
