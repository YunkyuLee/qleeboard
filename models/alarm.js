'use strict'
module.exports = (sequelize, DataTypes) => {
  const alarm = sequelize.define('Alarm', {
    post_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    user_srl: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    comments_count: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    comments_check_count: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  }, {
    classMethods: {},
    tableName: 'alarms',
    freezeTableName: true,
    paranoid: true,
    underscored: true,
    timestamps: true
  })

  alarm.associate = (models) => {
    models.Alarm.belongsTo(models.Post, {
      foreignKey: 'post_id',
      targetKey: 'id'
    })
  }
  return alarm
}
